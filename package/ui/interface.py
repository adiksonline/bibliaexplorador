# -*- coding: utf8 -*-

import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

"""This is the BibleInterface module."""

class BibleInterface(QMainWindow):
    """This the BibleInterface class."""
    def __init__(self):
        super(BibleInterface, self).__init__()
        self.initUi()

    def initUi(self):
        """Creates and loads the entire GUI for the program."""
        self.status = self.statusBar()
        self.centralWidget = QWidget()
        self.centralWidget.setObjectName("centralWidget")

        self.dictAndSearchDock = QDockWidget("Search", self)
        self.dictAndSearchDock.setObjectName("dictAndSearchDock")
        self.dictAndSearchWidget = QWidget()
        self.dictAndSearchTab = QTabWidget()
        """dictTab = QWidget()
        self.dictSearchEdit = QLineEdit()
        self.dictSearchButton = QPushButton("Find")
        self.dictList = QListWidget()
        self.dictBrowser = QTextBrowser()
        dictHLayout = QHBoxLayout()
        dictHLayout.addWidget(self.dictSearchEdit)
        dictHLayout.addWidget(self.dictSearchButton)
        dictLayout = QVBoxLayout()
        dictLayout.addLayout(dictHLayout)
        dictLayout.addWidget(self.dictList)
        dictLayout.addWidget(self.dictBrowser)
        dictTab.setLayout(dictLayout)"""
        #implementing the search tab
        self.searchTab = QWidget()
        self.searchEdit = QLineEdit()
        self.searchFromCombo = QComboBox()
        self.searchCheck = QCheckBox("&Match case")
        self.searchFromCombo.setToolTip("Start search from")
        self.searchToCombo = QComboBox()
        self.searchToCombo.setToolTip("End search at")
        self.searchButton = QPushButton()
        self.searchButton.setIcon(QIcon(":/search.png"))
        self.connect(self.searchEdit, SIGNAL("returnPressed()"), self.searchButton, SLOT("click()"))
        self.searchLabel = QLabel("Results")
        self.searchList = QListWidget()
        self.searchList.setAlternatingRowColors(True)
        searchHLayout = QHBoxLayout()
        searchFromToLayout = QHBoxLayout()
        searchHLayout.addWidget(self.searchEdit)
        searchHLayout.addWidget(self.searchButton)
        searchFromToLayout.addWidget(self.searchFromCombo)
        searchFromToLayout.addWidget(self.searchToCombo)
        self.searchBrowser = QTextBrowser()
        searchLayout = QVBoxLayout()
        searchLayout.addLayout(searchHLayout)
        searchLayout.addWidget(self.searchCheck)
        searchLayout.addLayout(searchFromToLayout)
        searchLayout.addWidget(self.searchLabel)
        searchLayout.addWidget(self.searchList)
        searchLayout.addWidget(self.searchBrowser)
        self.searchTab.setLayout(searchLayout)
        self.dictAndSearchTab.addTab(self.searchTab, "Search")
        #self.dictAndSearchTab.addTab(dictTab, "Dictionary")
        #this is getting cool
        dictAndSearchLayout = QHBoxLayout()
        dictAndSearchLayout.addWidget(self.dictAndSearchTab)
        self.dictAndSearchWidget.setLayout(dictAndSearchLayout)
        self.dictAndSearchDock.setWidget(self.dictAndSearchWidget)
        self.addDockWidget(Qt.RightDockWidgetArea, self.dictAndSearchDock)

        """self.commDock = QDockWidget("Commentary", self)
        self.commDock.setObjectName("commDock")
        self.commWidget = QWidget()
        self.commTree = QTreeWidget()
        self.commBrowser = QTextBrowser()
        commLayout = QVBoxLayout()
        commLayout.addWidget(self.commTree)
        commLayout.addWidget(self.commBrowser)
        self.commWidget.setLayout(commLayout)
        self.commDock.setWidget(self.commWidget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.commDock)"""

        self.notesBookmarksDock = QDockWidget("Notes and Bookmarks", self)
        self.notesBookmarksDock.setObjectName("notesBookmarksDock")
        self.notesBookmarksWidget = QWidget()
        self.notesBookmarksTab = QTabWidget()
        notesTab = QWidget()
        self.notesList = QListWidget()
        self.notesList.setAlternatingRowColors(True)
        self.notesList.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.notesNewButton = QPushButton("&New")
        self.notesDeleteButton = QPushButton("&Delete")
        self.notesBrowser = QTextBrowser()
        self.notesVerseBrowser = QTextBrowser()
        notesDialogBox = QDialogButtonBox()
        notesDialogBox.addButton(self.notesNewButton, QDialogButtonBox.ActionRole)
        notesDialogBox.addButton(self.notesDeleteButton, QDialogButtonBox.ActionRole)
        notesLayout = QVBoxLayout()
        notesLayout.addWidget(self.notesList)
        notesLayout.addWidget(notesDialogBox)
        notesLayout.addWidget(self.notesBrowser)
        notesLayout.addWidget(self.notesVerseBrowser)
        notesTab.setLayout(notesLayout)
        #this is the bookmarks widget
        bookmarksTab = QWidget()
        self.bookmarksList = QListWidget()
        self.bookmarksList.setAlternatingRowColors(True)
        self.bookmarksList.setContextMenuPolicy(Qt.ActionsContextMenu)
        bookmarksLayout = QHBoxLayout()
        bookmarksLayout.addWidget(self.bookmarksList)
        bookmarksTab.setLayout(bookmarksLayout)
        #combining the two widgets on a tab
        self.notesBookmarksTab.addTab(bookmarksTab, "Bookmarks")
        self.notesBookmarksTab.addTab(notesTab, "Notes")
        notesBookmarksLayout = QHBoxLayout()
        notesBookmarksLayout.addWidget(self.notesBookmarksTab)
        self.notesBookmarksWidget.setLayout(notesBookmarksLayout)
        self.notesBookmarksDock.setWidget(self.notesBookmarksWidget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.notesBookmarksDock)

        #the tab seperating the two sidescompareTab
        self.baseTab = QTabWidget()
        compareTab = QWidget()
        self.compareToolBar = QToolBar()
        self.compareToolBar.setObjectName("compareToolBar")
        self.compareTable = QTableWidget()
        #self.compareTable.setSelectionModel(QItemSelectionModel.Rows)
        compareLayout = QVBoxLayout()
        compareLayout.addWidget(self.compareTable)
        compareLayout.addWidget(self.compareToolBar)
        compareTab.setLayout(compareLayout)

        self.centralTab = QTabWidget()
        self.centralTab.setTabsClosable(True)
        self.centralTab.setMovable(True)

        self.baseTab.addTab(self.centralTab, "Bibles")
        self.baseTab.addTab(compareTab, "Compare")
        centralLayout = QHBoxLayout()
        centralLayout.addWidget(self.baseTab)
        self.centralWidget.setLayout(centralLayout)
        self.centralTab.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.setCentralWidget(self.centralWidget)

        for i in (self.notesBookmarksDock, self.dictAndSearchDock):
            self.connect(i, SIGNAL("visibilityChanged(bool)"), self.updateTableSize)
            self.connect(i, SIGNAL("topLevelChanged(bool)"), self.updateTableSize)

    def updateTableSize(self):
        """Not implemented."""
        pass




if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle(QStyleFactory.create("Cleanlooks"))
    app.setPalette(QApplication.style().standardPalette())
    bible = BibleInterface()
    bible.showMaximized()
    app.exec_()
