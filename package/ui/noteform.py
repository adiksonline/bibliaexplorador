# -*- coding: utf8 -*-
import sys
from PyQt4.QtGui import *
from PyQt4.QtCore import *

"""This is the Noteform module."""

class Noteform(QDialog):

    """This is thE Noteform class."""
    def __init__(self, parent = None):
        """Constructor."""
        super(Noteform, self).__init__(parent)
        self.initUi()

    def initUi(self):
        """
        Creates and loads the interface for the notes window."""
        self.setModal(True)
        self.setWindowFlags(Qt.Drawer)
        self.noteDiagEdit = QTextEdit()
        self.noteDiagEdit.setContextMenuPolicy(Qt.ActionsContextMenu)
        self.noteDiagButton = QPushButton()
        self.noteDiagButton.setFocusPolicy(Qt.NoFocus)
        self.noteDiagButton.setIcon(QIcon(":/accept.png"))
        layout = QVBoxLayout()
        layout.addWidget(self.noteDiagEdit)
        layout.addWidget(self.noteDiagButton)
        self.setLayout(layout)
        self.choose()

    def choose(self):
        """
        Displays the book, chapter and verse combos to select
        a verse to be added to a note."""
        self.chooseDialog = QDialog(self)
        self.chooseDialog.setObjectName("chooseDialog")
        self.chooseDialog.setWindowFlags(Qt.SplashScreen)
        self.bookCombo = QComboBox()
        self.chapterCombo = QComboBox()
        self.verseCombo = QComboBox()
        self.chooseAcceptButton = QPushButton()
        self.chooseAcceptButton.setIcon(QIcon(":/accept.png"))
        self.chooseRejectButton = QPushButton()
        self.chooseRejectButton.setIcon(QIcon(":/delete.png"))
        layout = QGridLayout()
        layout.addWidget(self.bookCombo, 0, 0)
        layout.addWidget(self.chapterCombo, 0, 1)
        layout.addWidget(self.verseCombo, 0, 2)
        layout.addWidget(self.chooseAcceptButton, 1, 1)
        layout.addWidget(self.chooseRejectButton, 1, 2)
        self.chooseDialog.setLayout(layout)
