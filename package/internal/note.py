# -*- coding: utf8 -*-
"""This is the Notes module."""

import linecache
from package.ui.noteform import *

HOME = QDir.homePath() + "/.bibliaexplorador"



class Notes(Noteform):
    """
    This is the Notes class."""

    def __init__(self, parent):
        """Constructor."""
        self.interface = parent
        super(Notes, self).__init__(parent)
        self.bibleBooks = self.interface.bible.bibleBooks
        self.connect(self.noteDiagButton, SIGNAL("clicked()"), self.saveNote)
        self.setupCombos()
        chooseAction = self.interface.createAction("Add a Verse", "noteadd", tip = "add verse to note", slot = self.chooseDialog.exec_)
        self.noteDiagEdit.addAction(chooseAction)
        self.setWindowTitle("Notes")
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        self.noteDiagEdit.setFocus()

    def setupCombos(self):
        """Sets up the book, chapter and verse combos for notes."""
        self.bookCombo.clear()
        self.bookCombo.addItems([i.attrib["bname"] for i in self.bibleBooks])
        self.chapterCombo.clear()
        self.chapterCombo.addItems([i.attrib["cnumber"] for i in self.bibleBooks[0].getiterator("CHAPTER")])
        self.verseCombo.addItems(map(unicode, range(1, int(list(self.bibleBooks[0].getiterator("CHAPTER"))[0].attrib["nverses"]))))
        self.connect(self.bookCombo, SIGNAL("currentIndexChanged(int)"), self.updateCombos)
        self.connect(self.chapterCombo, SIGNAL("currentIndexChanged(int)"), self.updateCombos)
        self.connect(self.chooseAcceptButton, SIGNAL("clicked()"), self.returnVerse)
        self.connect(self.chooseRejectButton, SIGNAL("clicked()"), self.chooseDialog.reject)
        self.connect(self, SIGNAL("rejected()"), self.noteDiagButton, SLOT("click()"))

    def updateCombos(self):
        """Sets the book, chapter and verse combos to the selected book, chapter and verse."""
        if self.sender() == self.bookCombo:
            self.chapterCombo.clear()
            self.chapterCombo.addItems(map(unicode, range(1, int(self.bibleBooks[self.bookCombo.currentIndex()].attrib["nchapters"]) + 1)))
        if self.sender() == self.chapterCombo:
            self.verseCombo.clear()
            chapterInd = self.chapterCombo.currentIndex()
            chapter = list(self.bibleBooks[self.bookCombo.currentIndex()].getiterator("CHAPTER"))[chapterInd]
            self.verseCombo.addItems(map(unicode, range(1, int(chapter.attrib["nverses"]) + 1)))

    def returnVerse(self):
        """
        Returns the hyperlink for the selected verse.
        It gets the current index of the combos and generates the hyperlink"""
        verse = "{0} {1} {2}".format(self.bookCombo.currentIndex(), self.chapterCombo.currentIndex(), self.verseCombo.currentIndex())
        self.noteDiagEdit.insertPlainText(" ")
        self.noteDiagEdit.insertHtml("<a href = '{0}'>{1}</a>&nbsp".format(verse, self.getDisplayText(verse)))
        self.chooseDialog.accept()

    def getDisplayText(self, verse):
        """
        Gets the URL from the hyperlink and generates a verse string,
        returns it."""
        vlist = map(int, verse.split(" "))
        return "{0} {1} : {2}".format(self.bibleBooks[vlist[0]].attrib["bname"], vlist[1] + 1, vlist[2] + 1)

    def loadNotes(self):
        """
        Gets saved notes from the notes folder and loads them
        accordingly."""
        self.disconnect(self.interface.notesList, SIGNAL("itemSelectionChanged()"), self.showNote)
        self.disconnect(self.interface.notesList, SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.openNote)
        self.disconnect(self.interface.notesBrowser, SIGNAL("anchorClicked(const QUrl&)"), self.displayVerse)
        self.disconnect(self.interface.notesBrowser, SIGNAL("highlighted(const QUrl&)"), self.displayVerseTip)
        self.interface.notesBrowser.clear()
        self.interface.notesVerseBrowser.clear()
        linecache.clearcache()
        self.notes = []
        noteDir = QDir("{0}/Notes".format(HOME))
        for fileInfo in noteDir.entryInfoList(QDir.Files, QDir.Name):
            if fileInfo.suffix() == "bnt":
                self.notes.append(fileInfo.completeBaseName())
        self.interface.notesList.clear()
        self.interface.notesList.addItems(self.notes)
        self.connect(self.interface.notesList, SIGNAL("itemSelectionChanged()"), self.showNote)
        self.connect(self.interface.notesList, SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.openNote)
        self.connect(self.interface.notesBrowser, SIGNAL("anchorClicked(const QUrl&)"), self.displayVerse)
        self.connect(self.interface.notesBrowser, SIGNAL("highlighted(const QUrl&)"), self.displayVerseTip)
        if not self.notes: self.interface.notesDeleteButton.setEnabled(False)

    def saveNote(self):
        """
        Saves a note."""
        f = QFile("{0}/Notes/{1}.bnt".format(HOME, self.currentTitle))
        ok = f.open(QIODevice.WriteOnly)
        if ok:
            stream = QTextStream(f)
            stream << self.noteDiagEdit.toHtml()
        else:
            QMessageBox.critical(self, "Error", "Unable to save note\n" + f.errorString())
        f.close()
        self.loadNotes()
        self.accept()

    def newNote(self):
        """
        Creates a new note.
        Asks the user for a title, if the title is valid,
        it creates a note with that title."""
        while True:
            title, ok = QInputDialog.getText(self, "New Note Title", "Enter the name for the new note:")
            if ok and title:
                if not (unicode(title) in self.notes):
                    noteFile = QFile("{0}/Notes/{1}.bnt".format(HOME, unicode(title)))
                    success = noteFile.open(QIODevice.WriteOnly)
                    noteFile.close
                    if success:
                        self.currentTitle = unicode(title)
                        break
            elif not ok:
                return
        self.noteDiagEdit.setHtml("")
        self.setWindowTitle("Note - {0}".format(self.currentTitle))
        self.show()
        self.interface.notesDeleteButton.setEnabled(True)

    def deleteNote(self):
        """
        Deletes the currently selected note."""
        if self.notes:
            ind = self.interface.notesList.currentRow()
            ok = QMessageBox.question(self, "Delete", "Are you sure", QMessageBox.Yes | QMessageBox.No)
            if ok == QMessageBox.Yes:
                success = QFile.remove("{0}/Notes/{1}.bnt".format(HOME, self.notes[ind]))
                if not success:
                    QMessageBox.critical(self, "Error", "Unable to remove note")
                self.loadNotes()
            if not self.notes: self.interface.notesDeleteButton.setEnabled(False)

    def openNote(self):
        """Gets the selected note from the notes folder and loads it appropriately.
        Here the note can be edited and deleted."""
        ind = self.interface.notesList.currentRow()
        text = "\n".join(linecache.getlines("{0}/Notes/{1}.bnt".format(HOME, self.notes[ind])))
        self.noteDiagEdit.setHtml(text.decode("utf8"))
        self.setWindowTitle("Note - {0}".format(self.notes[ind]))
        self.currentTitle = self.notes[ind]
        self.show()

    def showNote(self):
        """Displays a selected note in the notes viewer tab."""
        self.interface.notesVerseBrowser.clear()
        ind = self.interface.notesList.currentRow()
        if ind < self.interface.notesList.count():
            text = "\n".join(linecache.getlines("{0}/Notes/{1}.bnt".format(HOME, self.notes[ind])))
            self.interface.notesBrowser.setHtml(text.decode("utf8"))

    def displayVerse(self, verse):
        """Gets the URL of a selected verse in a note and converts it to book,
        chapter and verse index to display that verse in the verse viewer
        for notes.
        @param verse the URL for the verse."""
        version = unicode(self.interface.centralTab.tabText(self.interface.centralTab.currentIndex()))
        verse = map(int, verse.toString().split(" "))
        line = self.interface.static.verseToLine(verse)
        text = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, version), line)
        self.interface.notesVerseBrowser.setHtml(text)
        self.interface.notesBrowser.reload()

    def displayVerseTip(self, verse):
        """Gets the URL of a selected verse in a note and converts it to book,
        chapter and verse index to display that verse as a tool tip.
        @param verse the URL for the verse."""
        try:
            version = unicode(self.interface.centralTab.tabText(self.interface.centralTab.currentIndex()))
            verse = map(int, verse.toString().split(" "))
            line = self.interface.static.verseToLine(verse)
            text = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, version), line)
            self.interface.notesBrowser.setToolTip(text)
            self.interface.notesBrowser.reload()
        except:
            pass

