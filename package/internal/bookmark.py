# -*- coding: utf8 -*-

"""This is the Bookmark module"""
class Bookmarks(object):
    """This is the Bookmarks Class."""
    def __init__(self, parent):
        self.interface = parent

    def loadBookmarks(self):
        """Loads savde bookmarks."""
        self.bookmarkList = self.interface.bookmarks
        for i in self.bookmarkList:
            self.interface.bookmarksList.addItem("{0} {1} : {2}".format(self.interface.bookCombo.itemText(i[0]), i[1] + 1, i[2] + 1))

    def addBookmark(self):
        """Add a new bookmark.
        """
        book = self.interface.bookCombo.currentIndex()
        chapter = self.interface.chapterCombo.currentIndex()
        verse = self.interface.verseCombo.currentIndex()
        current = (book, chapter, verse)
        if current not in self.bookmarkList:
            self.bookmarkList.append(current)
            self.interface.bookmarksList.addItem("{0} {1} : {2}".format(self.interface.bookCombo.itemText(current[0]), current[1] + 1, current[2] + 1))
        self.interface.bookmarkDeleteAction.setEnabled(True)

    def deleteBookmark(self):
        """ Deletes a selected bookmark."""
        ind = self.interface.bookmarksList.currentRow()
        self.bookmarkList.remove(self.bookmarkList[ind])
        self.interface.bookmarksList.takeItem(ind)
        if not self.interface.bookmarksList.count():
            self.interface.bookmarkDeleteAction.setEnabled(False)

    def goto(self):
        """
        Loads the bookmark address onto the bible to set the current bok, chapter and verse to the bookmark address.S"""
        if self.interface.gotoToolBar.isEnabled():
            current = self.bookmarkList[self.interface.bookmarksList.currentRow()]
            j = 0
            for i in (self.interface.bookCombo, self.interface.chapterCombo, self.interface.verseCombo):
                i.setCurrentIndex(current[j])
                j += 1
