# -*- coding: utf8 -*-

"""This is the Compare module."""

import linecache
from PyQt4.QtCore import *
from PyQt4.QtGui import *

HOME = QDir.homePath() + "/.bibliaexplorador"



class Compare(object):
    """
    This is the Compare class.
    It compares selected book, chapter or verse of different bible versions."""
    def __init__(self, parent):
        self.interface = parent
        self.activeChecks = []

    def loadBibles(self):
        """
        Loads available bible versions from the bibles folder."""
        tempList = self.activeChecks[:]
        self.interface.compareToolBar.clear()
        self.interface.compareTable.setColumnCount(0)
        self.interface.compareTable.setRowCount(0)
        self.activeChecks = []
        books = []
        booksDir = QDir("{0}/Bibles".format(HOME))
        for book in booksDir.entryInfoList(QDir.Files, QDir.Name):
            if book.suffix() == "ont":
                i = unicode(book.completeBaseName())
                books.append(i)
                check = QAction(i, self.interface)
                check.setCheckable(True)
                self.interface.connect(check, SIGNAL("toggled(bool)"), self.updateTableColumn)
                check.setChecked(i in tempList) #since others are already false. it wont affect them, else better use if check so as to avoid logic errors.
                self.interface.compareToolBar.addAction(check)
        self.interface.connect(self.interface.compareTable, SIGNAL("currentCellChanged(int, int, int, int)"), self.updateVerseCombo)

    def updateVerseCombo(self):
        """
        Sets the selected verse in the bible table to the current verse in the verse combo."""
        self.interface.verseCombo.setCurrentIndex(self.interface.compareTable.currentRow())

    def updateTableColumn(self, state):
        """
        Adds a new bible table to the compare tab and loads in the selected bible version.
        @param state the state of the checkbox( checked/unchecked )."""
        if state:
            self.interface.compareState = True
            self.interface.gotoToolBar.setEnabled(self.interface.compareState)
            self.interface.editToolBar.setEnabled(self.interface.compareState)
            self.interface.fontToolBar.setEnabled(self.interface.compareState)
            self.activeChecks.append(self.interface.sender().text())
            self.interface.compareTable.insertColumn(self.interface.compareTable.columnCount())
            self.interface.compareTable.setHorizontalHeaderLabels(self.activeChecks)
            self.interface.updateCurrentBible()

        else:
            ind = self.activeChecks.index(self.interface.sender().text())
            self.interface.compareTable.removeColumn(ind)
            self.activeChecks.remove(self.interface.sender().text())
            if not self.interface.compareTable.columnCount():
                self.interface.compareTable.setRowCount(0)
                self.interface.compareState = False
                self.interface.gotoToolBar.setEnabled(self.interface.compareState)
                self.interface.editToolBar.setEnabled(self.interface.compareState)
                self.interface.fontToolBar.setEnabled(self.interface.compareState)
        self.interface.compareTable.horizontalHeader().resizeSections(QHeaderView.Stretch)

    def goto(self, book, chapter, verse):
        """Move to selected book, chaper and verse.
        @param book the book combo index.
        @param chapter the chapter combo index.
        @param verse the verse combo index."""
        testing = QTextDocument() # Get a more legal alternative
        bibleBooks = self.interface.bible.bibleBooks
        bookElem = bibleBooks[book]
        bibleChapter = list(bookElem.getiterator("CHAPTER"))[chapter]
        self.interface.compareTable.setRowCount(int(bibleChapter.attrib["nverses"]))
        j = 1
        for i in range(int(bibleChapter.attrib["line"]), int(bibleChapter.attrib["line"]) + int(bibleChapter.attrib["nverses"])):
            for k in range(self.interface.compareTable.columnCount()):
                text = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, self.activeChecks[k]), i)
                testing.setHtml(text)
                text = testing.toPlainText() + "\n"
                item1 = QTableWidgetItem(text)
                item1.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
                self.interface.compareTable.setItem(j-1, k, item1)
            j += 1
        self.interface.compareTable.selectRow(verse)
        self.interface.compareTable.horizontalHeader().resizeSections(QHeaderView.Stretch)
        self.interface.compareTable.resizeRowsToContents()
        self.setTableTip()

    def setTableTip(self):
        """
        Sets the tooltip for the comparison table.
        """
        self.interface.compareTable.setStatusTip("{0} {1} {2}".format(self.interface.bookCombo.currentIndex(), self.interface.chapterCombo.currentIndex(), self.interface.verseCombo.currentIndex()))
