# -*- coding: utf8 -*-

"""
This is the bible module.
It takes care of all the capabilities needed by the bible window"""

import linecache
from xml.etree.ElementTree import ElementTree
from PyQt4.QtGui import *
from PyQt4.QtCore import *



HOME = QDir.homePath() + "/.bibliaexplorador"

class Bible(object):
    """
    This is the Bible class."""
    def __init__(self, parent):
        """
        Bible constructor."""
        self.interface = parent
        self.booksOpen = []
        self.currentBible = None
        tree = ElementTree()
        books = "{0}/Settings/books.xml".format(HOME)
        if not QFile.exists(books):
            raise IOError("Could not locate the Books config file. Will now exit")
        bibleXml = tree.parse(books)
        self.bibleBooks = list(bibleXml.getiterator("BIBLEBOOK"))

    def loadBibles(self):
        """
        This method gets the list of available bibles and sets them in the bible menu."""
        self.interface.openMenu.clear()
        self.interface.versionToolBar.clear()
        self.interface.compare.loadBibles()
        books = []
        booksDir = QDir("{0}/Bibles".format(HOME))
        for book in booksDir.entryInfoList(QDir.Files, QDir.Name):
            if book.suffix() == "ont":
                i = unicode(book.completeBaseName())
                books.append(i)
                action = self.interface.createAction(i, tip = i, slot = self.openBook)
                action.setData(QVariant(i))
                font = action.font()
                font.setBold(True)
                action.setFont(font)
                self.interface.openMenu.addAction(action)
                if i.lower() == "kjv":
                    self.interface.openMenu.setDefaultAction(action)
                self.interface.versionToolBar.addAction(action)

    def addNewTab(self, book):
        """
        This method creates a new tab.
        @param book load selected version."""
        tab = QWidget()
        tabTable = QTableWidget()
        tabTable.horizontalHeader().setStretchLastSection(True)
        tabTable.setWordWrap(True)
        tabTable.setColumnCount(1)
        tabTable.setHorizontalHeaderLabels(["Contents", ])
        layout = QHBoxLayout()
        layout.addWidget(tabTable)
        tab.setLayout(layout)
        self.interface.centralTab.addTab(tab, book)
        self.interface.centralTab.setCurrentIndex(self.interface.centralTab.count() - 1)
        return tabTable

    def openBook(self):
        """
        This method adds a new tab if the book is not already opened and
        lauches the bible onto the already created tab widget."""
        book = self.interface.sender().data().toString()
        if book in [i[0] for i in self.booksOpen]:
            self.interface.centralTab.setCurrentIndex([i[0] for i in self.booksOpen].index(book))
        else:
            self.interface.centralState = True
            newTable = self.addNewTab(book)
            self.booksOpen.append([book, newTable])
            j = 1
            bibleChapter = self.bibleBooks[0].find("CHAPTER")
            self.booksOpen[-1][1].setRowCount(int(bibleChapter.attrib["nverses"]))
            for i in xrange(int(bibleChapter.attrib["line"]), int(bibleChapter.attrib["line"]) + int(bibleChapter.attrib["nverses"])):
                text = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, book), i)
                item1 = QTableWidgetItem(text)
                item1.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
                self.booksOpen[-1][1].setItem(j-1, 0, item1)
                j += 1
            self.interface.resetBibleCombos()
            self.interface.connect(self.booksOpen[-1][1], SIGNAL("currentCellChanged(int, int, int, int)"), self.interface.updateVerseCombo)
            self.interface.centralTab.setTabsClosable(True)
            for i in (self.interface.gotoToolBar, self.interface.closeAction, self.interface.editToolBar, self.interface.fontToolBar, self.interface.searchList):
                i.setEnabled(True)
            self.interface.updateCurrentFont()
            self.booksOpen[-1][1].setStatusTip("0 0 0")
            current = list(QString("0 0 0"))
            self.booksOpen[-1][1].setHorizontalHeaderLabels(["{0} {1}".format(self.interface.bookCombo.itemText(current[0].toInt()[0]), current[1].toInt()[0] + 1)])

    def closeTab(self, ind):
        """ Closes the current bible table."""
        if self.interface.centralTab.count() > 1:
            self.booksOpen.remove(self.booksOpen[ind])
            self.interface.centralTab.removeTab(ind)
            if self.interface.centralTab.count() == 1:
                self.interface.closeAction.setEnabled(False)
                self.interface.centralTab.setTabsClosable(False)
            if not self.interface.centralTab.count():
                for i in (self.interface.gotoToolBar, self.interface.editToolBar, self.interface.fontToolBar, self.interface.searchList):
                    i.setEnabled(False)

    def resort(self, oi, ni):
        cur = self.booksOpen.pop(oi)
        self.booksOpen.insert(ni, cur)

    def goto(self, book, chapter, verse):
        """
        This method loads the supplied verse.
        @param book the bible book.
        @param chapter the bible chapter.
        @param verse the bible verse."""
        testing = QTextDocument() # Reimplement this with a more legal alternative
        bookElem = self.bibleBooks[book]
        bibleChapter = list(bookElem.getiterator("CHAPTER"))[chapter]
        self.booksOpen[self.interface.centralTab.currentIndex()][1].setRowCount(int(dict(bibleChapter.items())["nverses"]))
        j = 1
        for i in xrange(int(bibleChapter.attrib["line"]), int(bibleChapter.attrib["line"]) + int(bibleChapter.attrib["nverses"])):
            text = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, self.interface.centralTab.tabText(self.interface.centralTab.currentIndex())), i)
            testing.setHtml(text)
            text = testing.toPlainText() + "\n"
            item1 = QTableWidgetItem(text)
            item1.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable)
            self.booksOpen[self.interface.centralTab.currentIndex()][1].setItem(j-1, 0, item1)
            j += 1
        self.booksOpen[self.interface.centralTab.currentIndex()][1].selectRow(verse)
        self.booksOpen[self.interface.centralTab.currentIndex()][1].resizeRowsToContents()
        self.setTableTip()

    def setTableTip(self):
        """Sets the tooltip for the bible table"""
        self.booksOpen[self.interface.centralTab.currentIndex()][1].setStatusTip("{0} {1} {2}".format(self.interface.bookCombo.currentIndex(), self.interface.chapterCombo.currentIndex(), self.interface.verseCombo.currentIndex()))
