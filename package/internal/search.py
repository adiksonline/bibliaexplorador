# -*- coding: utf8 -*-
"""This is the Search module."""

import linecache
from PyQt4.QtCore import QDir, QObject, QCoreApplication

HOME = QDir.homePath() + "/.bibliaexplorador"



class Search(QObject):

    """
    This is the Search class."""
    def __init__(self, parent):
        super(Search, self).__init__()
        self.interface = parent
        self.searchText = ""

    def searchWord(self, text, start, end):
        """
        Performs the search.
        Given the start line, end line and the search word.
        @param text search word.
        @param start line number of the search start point.
        @param end line number of the search end point."""
        #TODO: gotta improve on this search algorithm. still to put match whole words and preferencial priority for multiple words or sentences.
        start = int(self.interface.bible.bibleBooks[start].attrib["line"])
        end = int(self.interface.bible.bibleBooks[end + 1].attrib["line"]) if end < 65 else int(list(self.interface.bible.bibleBooks[-1].getiterator("CHAPTER"))[-1].attrib["line"]) + int(list(self.interface.bible.bibleBooks[-1].getiterator("CHAPTER"))[-1].attrib["nverses"])
        self.foundVerses = []
        self.searchText = text
        self.interface.searchEdit.setEnabled(False)
        self.interface.searchButton.setEnabled(False)
        self.interface.searchLabel.setText("Searching...")
        for line in xrange(start, end):
            version = str(self.interface.centralTab.tabText(self.interface.centralTab.currentIndex()))
            if self.interface.searchCheck.isChecked():
                text = str(text)
                curVerse = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, version), line)
            else:
                text = str(text).lower()
                curVerse = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, version), line).lower()
            if text in curVerse:
                i = self.interface.static.lineToVerse(line)
                self.foundVerses.append(i)
                self.interface.searchList.addItem("{0} {1} : {2}".format(self.interface.bookCombo.itemText(i[0]), i[1] + 1, i[2] + 1))
                QCoreApplication.processEvents()
        if not self.foundVerses:
            self.interface.searchList.addItems(["No Result Found", ])
        self.interface.searchLabel.setText("{0} Result(s) Found".format(len(self.foundVerses) if self.foundVerses else 0))
        self.interface.searchEdit.setEnabled(True)
        self.interface.searchButton.setEnabled(True)

    def goto(self):
        """
        Gets the book, chapter and verse index of a search
        result and set the current bible to that address."""
        if self.foundVerses and self.interface.gotoToolBar.isEnabled():
            verse = self.foundVerses[self.interface.searchList.currentRow()]
            j = 0
            for i in (self.interface.bookCombo, self.interface.chapterCombo, self.interface.verseCombo):
                i.setCurrentIndex(verse[j])
                j += 1

    def showVerse(self):
        """
        Gets the book, chapter and vere index of a s search
        result and displays it in the verse viewer."""
        if self.foundVerses and self.interface.searchList.currentRow() < len(self.foundVerses):
            version = str(self.interface.centralTab.tabText(self.interface.centralTab.currentIndex()))
            verse = self.foundVerses[self.interface.searchList.currentRow()]
            line = self.interface.static.verseToLine(verse)
            text = linecache.getline("{0}/Bibles/{1}.ont".format(HOME, version), line)
            text = text.replace(self.searchText, "<font color=red><b>{0}</b></font>".format(self.searchText))
            self.interface.searchBrowser.setHtml(text)
