# -*- coding: utf8 -*-

"""This is the module for the Static class."""

class Static(object):
    """Static class."""
    def __init__(self, parent):
        """Constructor"""
        self.interface = parent
        self.bibleBooks = self.interface.bible.bibleBooks

    def verseToLine(self, verse):
        """Converts the list of indexes to the line number of that particular verse.
        @param verse selected verse"""
        book = self.bibleBooks[verse[0]]
        chapterNum = int(dict(list(book.getiterator("CHAPTER"))[verse[1]].items())["line"])
        return chapterNum + verse[2]

    def lineToVerse(self, line):
        """Converts the particular verse to an index for the book, chapter and verse.
        @param line line number."""
        if int(dict(self.bibleBooks[-1].items())["line"]) < line:
            verse = [len(self.bibleBooks) - 1]
            chapter = list(self.bibleBooks[-1].getiterator("CHAPTER"))
            if int(dict(chapter[-1].items())["line"]) == line:
                verse.append(len(chapter) - 1)
                verse.append(0)
                return verse

            elif int(dict(chapter[-1].items())["line"]) < line:
                verse.append(len(chapter) - 1)
                verse.append(line - int(dict(chapter[-1].items())["line"]))
                return verse
            else:
                for j in chapter:
                    if int(dict(j.items())["line"]) == line:
                        verse.append(int(dict(j.items())["cnumber"]) - 1)
                        verse.append(0)
                        return verse
                    elif int(dict(j.items())["line"]) > line:
                        verse.append(chapter.index(j) - 1)
                        verse.append(line - int(dict(chapter[chapter.index(j) - 1].items())["line"]))
                        return verse
        else:
            for i in self.bibleBooks:
                if int(dict(i.items())["line"]) == line:
                    verse = [self.bibleBooks.index(i), 0, 0]
                    return verse
                elif int(dict(i.items())["line"]) > line:
                    verse = [self.bibleBooks.index(i) - 1]
                    book = self.bibleBooks[self.bibleBooks.index(i) - 1]
                    if line > int(dict(list(book.getiterator("CHAPTER"))[-1].items())["line"]):
                        verse.append(len(list(book.getiterator("CHAPTER"))) - 1)
                        verse.append(line - int(dict(list(book.getiterator("CHAPTER"))[-1].items())["line"]))
                        return verse
                    else:
                        for j in book.getiterator("CHAPTER"):
                            if int(dict(j.items())["line"]) == line:
                                verse.append(int(dict(j.items())["cnumber"]) - 1)
                                verse.append(0)
                                return verse
                            elif int(dict(j.items())["line"]) > line:
                                verse.append(int(dict(j.items())["cnumber"]) - 2)
                                chapter = list(book.getiterator("CHAPTER"))[int(dict(j.items())["cnumber"]) - 2]
                                verse.append(line - int(dict(chapter.items())["line"]))
                                return verse

    def addActions(self, target, actions):
        """
        Adds  each of the actions to specified widget.
        @param target the widget.
        @param actions the list of actions."""
        for action in actions:
            if action is None:
                target.addSeparator()
            else:
                target.addAction(action)
