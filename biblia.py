# -*- coding: utf8 -*-
"""This is the Biblia module."""

from package.ui.interface import *
from package.ui.resource_rc import *
from package.internal.bible import *
from package.api.static import Static
from package.internal.search import Search
from package.internal.bookmark import Bookmarks
from package.internal.compare import Compare
from package.internal.note import Notes


class Biblia(BibleInterface):
    """Binds all classes of the BibliaXplorador together and controls data flow of the entire application.
    """
    def __init__(self, splash):
        splash.showMessage("Generating GUI . . .")
        super(Biblia, self).__init__()
        self.setWindowTitle("Biblia Explorador")
        splash.showMessage("Loading the Bible Module . . .")
        self.bible = Bible(self)
        self.static = Static(self)
        splash.showMessage("Loading the Search Module . . .")
        self.search = Search(self)
        self.mySearch = MySearch()
        self.mySearch.setAutoDelete(False)
        self.bookmark = Bookmarks(self)
        splash.showMessage("Loading the Note Module . . .")
        self.note = Notes(self)
        self.note.loadNotes()
        splash.showMessage("Loading the Compare Module . . .")
        self.compare = Compare(self)
        self.compare.loadBibles()
        splash.showMessage("Initializing the Menus . . .")
        self.initMenu()
        self.clipboard = QApplication.clipboard()
        splash.showMessage("Setting up Connections . . .")
        self.connect(self.baseTab, SIGNAL("currentChanged(int)"), self.updateCentral)
        self.connect(self.centralTab, SIGNAL("currentChanged(int)"), self.updateBibleCombos)
        self.connect(self.notesNewButton, SIGNAL("clicked()"), self.note.newNote)
        self.connect(self.notesDeleteButton, SIGNAL("clicked()"), self.note.deleteNote)
        splash.showMessage("Loading the Default Bible . . .")
        if self.openMenu.actions():
            try:
                self.openMenu.defaultAction().trigger()
            except:
                self.openMenu.actions()[0].trigger()
            self.closeAction.setEnabled(False)
            self.centralTab.setTabsClosable(False)
            self.restoreAll()
            splash.showMessage("Initializing Bookmarks . . .")
            self.initBookmarks()
            self.bindDocksToMenu()
        else:
            QMessageBox.critical(self,"No Bible Found", "A Bible is needed to Launch the Application")
            sys.exit(1)
        self.myColors()

    def myColors(self):
        """
        Reads the css file and applies accordingly."""
        try:
            s = open("{0}/.bibliaexplorador/Settings/mycss.css".format(QDir.homePath())).read()
            self.setStyleSheet(s)
        except:
            QMessageBox.warning(self, "CSS Not Found", "Could not locate the CSS file, Using default Stylesheet")

    def restoreAll(self):
        """
        Restores the state, geometry, font and bible verse to the state it was upon the last exit."""
        settings = QSettings()
        self.restoreGeometry(settings.value("mainwindow/geometry").toByteArray())
        self.restoreState(settings.value("mainwindow/state").toByteArray())
        self.bookmarks = eval(str(settings.value("bookmarks", "[]").toString()))
        current = settings.value("gotoCombos", QString("0 0 0")).toString()
        current = current.split(" ")
        j = 0
        for i in (self.bookCombo, self.chapterCombo, self.verseCombo):
            i.setCurrentIndex(current[j].toInt()[0])
            j += 1
        self.bible.booksOpen[self.centralTab.currentIndex()][1].setHorizontalHeaderLabels(["{0} {1}".format(self.bookCombo.itemText(current[0].toInt()[0]), current[1].toInt()[0] + 1)])
        fname = settings.value("font/family", self.bible.booksOpen[self.centralTab.currentIndex()][1].font().family()).toString()
        self.fontName.setCurrentIndex(self.fontName.findText(fname, Qt.MatchStartsWith))
        self.fontSize.setEditText(settings.value("font/size", "11").toString())
        self.fontBoldAction.setChecked(settings.value("font/bold", False).toBool())
        self.fontItalicAction.setChecked(settings.value("font/italic", False).toBool())

    def initMenu(self):
        """Initializes the menu and creates the actions."""
        self.fileMenu = self.menuBar().addMenu("&File")
        self.openMenu = self.fileMenu.addMenu(QIcon(":/appicon.png"), "&Open Book")
        ##########
        self.editMenu = self.menuBar().addMenu("&Edit")
        self.copyVerseAction = self.createAction("Copy Selected &Verse", "copy", QKeySequence.Copy,
            "copy the current verse", slot = self.copyVerse)
        self.copyChapterAction = self.createAction("Copy Current &Chapter", "copy", "Ctrl+Shift+C",
            "copy the current chapter", slot = self.copyChapter)
        self.static.addActions(self.editMenu, (self.copyVerseAction, self.copyChapterAction))
        self.windowMenu = self.menuBar().addMenu("&Windows")
        self.helpMenu = self.menuBar().addMenu("&Help")
        aboutAction = self.createAction("&About", "about", tip = "launch the about", slot = self.aboutSoft)
        helpAction = self.createAction("&Help", "help", QKeySequence.HelpContents, "launch the help", slot = self.helpSoft)
        self.static.addActions(self.helpMenu, (helpAction, aboutAction))
        bookmarkAddAction = self.createAction("&Bookmark Selected Verse", icon = "bookmark", tip = "Add current verse to bookmark", slot = self.bookmark.addBookmark)
        self.bookmarkDeleteAction = self.createAction("&Delete", tip = "Delete current bookmark", slot = self.bookmark.deleteBookmark)

        self.static.addActions(self.centralTab, (bookmarkAddAction, self.copyVerseAction, self.copyChapterAction, aboutAction))
        self.static.addActions(self.bookmarksList, (self.bookmarkDeleteAction, ))
        self.closeAction = self.createAction("&Close Current", "close", "Ctrl+W", tip = "close the current book", slot = lambda: self.bible.closeTab(self.centralTab.currentIndex()))
        self.reloadAction = self.createAction("&Reload Books", "reload", QKeySequence.Refresh, tip = "Reloads the list of bibles", slot = self.bible.loadBibles)
        quitAction = self.createAction("&Quit", shortcut = "Ctrl+Q", tip = "Exit the application", slot = self.close)
        self.static.addActions(self.fileMenu, (self.closeAction, self.reloadAction, None, quitAction))
        ###############
        self.connect(self.centralTab, SIGNAL("tabCloseRequested(int)"), self.bible.closeTab)
        self.connect(self.centralTab.tabBar(), SIGNAL("tabMoved(int, int)"), self.bible.resort)
        ##############

        self.setBibleCombos()
        self.fileToolBar = self.addToolBar("File")
        self.fileToolBar.setObjectName("fileToolBar")
        self.editToolBar = self.addToolBar("Edit")
        self.editToolBar.setObjectName("editToolBar")
        self.editToolBar.setAllowedAreas(Qt.LeftToolBarArea)
        self.prevBookAction = self.createAction("Previous Book", "prevbook", tip = "open the previous book", slot = self.openPrevBook)
        self.prevChapAction = self.createAction("Previous Chapter", "prevchap", tip = "open the previous chapter", slot = self.openPrevChap)
        self.nextBookAction = self.createAction("Next Book", "nextbook", tip = "open the next book", slot = self.openNextBook)
        self.nextChapAction = self.createAction("Next Chapter", "nextchap", tip = "open the next chapter", slot = self.openNextChap)
        self.static.addActions(self.editToolBar, (self.prevBookAction, self.prevChapAction, self.nextChapAction, self.nextBookAction))
        self.fontToolBar = self.addToolBar("Fonts")
        self.fontToolBar.setObjectName("fontToolBar")
        self.fontName = QFontComboBox()
        self.fontSize = QComboBox()
        self.fontSize.addItems(map(str, range(5, 31)))
        self.fontSize.setEditable(True)
        self.connect(self.fontName, SIGNAL("currentFontChanged(const QFont&)"), self.updateCurrentFont)
        self.connect(self.fontSize, SIGNAL("currentIndexChanged(const QString&)"), self.updateCurrentFont)
        self.connect(self.fontSize, SIGNAL("editTextChanged(const QString&)"), self.updateCurrentFont)
        self.fontBoldAction = self.createAction("Font &Bold", icon = "bold", checkable = True, tip = "Make font bold", slot = self.updateCurrentFont, signal = "toggled(bool)")
        self.fontItalicAction = self.createAction("Font &Italic", icon = "italic", checkable = True, tip = "Font in italic", slot = self.updateCurrentFont, signal = "toggled(bool)")
        self.fontToolBar.addWidget(self.fontName)
        self.fontToolBar.addWidget(self.fontSize)
        self.static.addActions(self.fontToolBar, (self.fontBoldAction, self.fontItalicAction))
        self.static.addActions(self.fileToolBar, (self.closeAction, self.reloadAction))
        self.versionToolBar = self.addToolBar("Versions")
        self.versionToolBar.setObjectName("versionToolBar")
        self.bible.loadBibles()
        self.resetSearchWindow()
        self.bindDocksToMenu()

    def bindDocksToMenu(self):
        """
        Connects the docks to the menu by creating a list containing the dock names as Action Listeners to respective docks."""
        self.windowMenu.clear()
        for i in (self.versionToolBar, self.fileToolBar, self.gotoToolBar, self.fontToolBar, self.editToolBar, self.notesBookmarksDock, self.dictAndSearchDock):
            self.windowMenu.addAction(i.toggleViewAction())

    def setBibleCombos(self):
        """
        Creates a the Bible menus"""
        self.gotoToolBar = self.addToolBar("Goto")
        self.gotoToolBar.setObjectName("gotoToolBar")
        self.bookCombo = QComboBox()
        self.chapterCombo = QComboBox()
        self.verseCombo = QComboBox()
        self.resetBibleCombos()
        self.gotoToolBar.addWidget(self.bookCombo)
        self.gotoToolBar.addWidget(self.chapterCombo)
        self.gotoToolBar.addWidget(self.verseCombo)

        for i in (self.bookCombo, self.chapterCombo, self.verseCombo):
            #, self.searchList, self.bookmarkDeleteAction
            self.connect(i, SIGNAL("currentIndexChanged(int)"), self.updateCurrentBible)
        self.centralState = False
        self.compareState = False
        self.gotoToolBar.setEnabled(self.centralState)

    def resetBibleCombos(self):
        """Add the Bible books, chapters and verses to the Bible Combos that method setBibleCombos created."""
        self.bookCombo.clear()
        self.bookCombo.addItems([dict(i.items())["bname"] for i in self.bible.bibleBooks])
        self.chapterCombo.clear()
        self.chapterCombo.addItems(map(str, range(1, int(dict(self.bible.bibleBooks[0].items())["nchapters"]) + 1)))
        self.verseCombo.clear()
        self.verseCombo.addItems(map(str, range(1, int(dict(self.bible.bibleBooks[0].find("CHAPTER").items())["nverses"]) + 1)))

    def createAction(self, text, icon=None, shortcut=None, tip=None, checkable=False, slot=None, signal="triggered()"):
        """
        Creates an action and returns the action.
        @param text displays what the action does
        @param icon filename of the icon
        @param shortcut keyboard shortcut for this action
        @param tip tooltip for the action
        @param checkable checks if the action will be checkable or not
        @param slot listens to action coming from the signal it is connected to
        @param signal the action selected
        @return action the action created."""
        action = QAction(text, self)
        if icon is not None:
            action.setIcon(QIcon(":/%s.png" % icon))
        if shortcut is not None:
            action.setShortcut(shortcut)
        if tip is not None:
            action.setToolTip(tip)
            action.setStatusTip(tip)
        if slot is not None:
            self.connect(action, SIGNAL(signal), slot)
        if checkable:
            action.setCheckable(True)
        return action

    def copyVerse(self):
        """Copies selected verse(s) to the clipboard"""
        text = "{0} {1}:{2} - {3}".format(self.bookCombo.currentText(), self.chapterCombo.currentText(), self.verseCombo.currentText(), self.bible.booksOpen[self.centralTab.currentIndex()][1].currentItem().text())
        self.clipboard.setText(text)

    def copyChapter(self):
        """Copies selected chapter to the clipboard"""
        text = "{0} {1}:\n".format(self.bookCombo.currentText(), self.chapterCombo.currentText())
        for i in range(self.bible.booksOpen[self.centralTab.currentIndex()][1].rowCount()):
            text += "v{0} - {1}\n".format(i + 1, self.bible.booksOpen[self.centralTab.currentIndex()][1].item(i, 0).text())
        self.clipboard.setText(text)

    def aboutSoft(self):
        """About the Software"""
        QMessageBox.about(self, "About", """<p><font color = "red">BibliaExplorador is a complete and
            sophisticated bible software designed for our daily needs of the word of God<br/><br/></font></p>
            <h2>Authors:</h2>
            <ul>
            <li>Adio Kingsley O - Main Programmer
            <li>Itimi Richard O - Co Developer
            <li>Popoola Abayomi V -
            <li>Elesemoyo Isaac O -
            </ul>
            <br/>
            <h2>Special Thanks to:</h2>
            <ul>
            <li>Dr Odejobi O
            <li>Mr Akinade O
            </ul>
            """)

    def helpSoft(self):
        """Assists you on how to use the software"""
        helpDiag = QDialog(self)
        helpDiag.setWindowTitle("Help Contents")
        helpBrowser = QTextBrowser()
        toolBar = QToolBar()
        faction = self.createAction("Forward", "nextchap", slot = helpBrowser.forward)
        baction = self.createAction("Backward", "prevchap", slot = helpBrowser.backward)
        self.static.addActions(toolBar, (baction, faction))
        layout = QVBoxLayout()
        layout.addWidget(toolBar)
        layout.addWidget(helpBrowser)
        helpDiag.setLayout(layout)
        helpBrowser.setSearchPaths([":/help"])
        helpBrowser.setSource(QUrl("file:index.html"))
        helpDiag.resize(640, 640)
        helpDiag.exec_()
        #helpDiag.show()

    def resizeEvent(self, event):
        """resizes all display proportionate to the current size of the window by calling method updateTableSize().
        @param event the resize event."""
        self.updateTableSize()

    def updateTableSize(self):
        """
        Ensures that the table maintains it size."""
        if self.bible.booksOpen:
            self.bible.booksOpen[self.centralTab.currentIndex()][1].horizontalHeader().resizeSections(QHeaderView.Stretch)
            self.bible.booksOpen[self.centralTab.currentIndex()][1].resizeRowsToContents()

    def updateCentral(self, ind):
        """Restores the last state of the Bible and Compare Tabs when switching between them.
        @param ind index of the current tab."""
        if ind == 0:
            self.copyVerseAction.setEnabled(True)
            self.copyChapterAction.setEnabled(True)
            self.searchTab.setEnabled(True)
            self.versionToolBar.setEnabled(True)
            self.fileToolBar.setEnabled(True)
            self.gotoToolBar.setEnabled(self.centralState)
            self.editToolBar.setEnabled(self.centralState)
            self.fontToolBar.setEnabled(self.centralState)
            self.updateBibleCombos(0)
        else:
            self.copyVerseAction.setEnabled(False)
            self.copyChapterAction.setEnabled(False)
            self.searchTab.setEnabled(False)
            self.fileToolBar.setEnabled(False)
            self.versionToolBar.setEnabled(False)
            self.gotoToolBar.setEnabled(self.compareState)
            self.editToolBar.setEnabled(self.compareState)
            self.fontToolBar.setEnabled(self.compareState)
            if self.compareTable.columnCount() <= len(self.compare.activeChecks) and self.compareTable.columnCount():
                current = self.compareTable.statusTip().split(" ")
                j = 0
                for i in (self.bookCombo, self.chapterCombo, self.verseCombo):
                    i.setCurrentIndex(current[j].toInt()[0])
                    j += 1

    def updateBibleCombos(self, int):
        """
        Get the current bible's book, chapter and verse and uses it in updating the combos."""
        if self.centralTab.count() <= len(self.bible.booksOpen) and self.centralTab.count():
            current = self.bible.booksOpen[self.centralTab.currentIndex()][1].statusTip().split(" ")
            j = 0
            for i in (self.bookCombo, self.chapterCombo, self.verseCombo):
                i.setCurrentIndex(current[j].toInt()[0])
                j += 1
            self.bible.booksOpen[self.centralTab.currentIndex()][1].setHorizontalHeaderLabels(["{0} {1}".format(self.bookCombo.itemText(current[0].toInt()[0]), current[1].toInt()[0] + 1)])
            self.updateCurrentFont()

    def updateVerseCombo(self):
        """Changes the verse combo to the currently selected verse."""
        self.verseCombo.setCurrentIndex(self.bible.booksOpen[self.centralTab.currentIndex()][1].currentRow())

    def updateCurrentBible(self):
        """
        Get current settings for all combos and call the goto method to update the bible table."""
        if self.sender() == self.verseCombo:
            if self.baseTab.currentIndex() == 0:
                self.bible.booksOpen[self.centralTab.currentIndex()][1].selectRow(self.verseCombo.currentIndex())
        if self.sender() == self.bookCombo:
            self.chapterCombo.clear()
            self.chapterCombo.addItems(map(str, range(1, int(dict(self.bible.bibleBooks[self.bookCombo.currentIndex()].items())["nchapters"]) + 1)))
        if self.sender() == self.chapterCombo:
            self.verseCombo.clear()
            chapterInd = self.chapterCombo.currentIndex()
            chapter = list(self.bible.bibleBooks[self.bookCombo.currentIndex()].getiterator("CHAPTER"))[chapterInd]
            self.verseCombo.addItems(map(str, range(1, int(dict(chapter.items())["nverses"]) + 1)))
        if self.baseTab.currentIndex() == 0:
            self.bible.goto(self.bookCombo.currentIndex(), self.chapterCombo.currentIndex(), self.verseCombo.currentIndex())
            self.updateBibleCombos(1)
        else:
            self.compare.goto(self.bookCombo.currentIndex(), self.chapterCombo.currentIndex(), self.verseCombo.currentIndex())
            self.updateCurrentFont()
        self.prevBookAction.setEnabled(False) if self.bookCombo.currentIndex() == 0 else self.prevBookAction.setEnabled(True)
        self.prevChapAction.setEnabled(False) if self.chapterCombo.currentIndex() == 0 else self.prevChapAction.setEnabled(True)
        self.nextBookAction.setEnabled(False) if self.bookCombo.currentIndex() == (self.bookCombo.count() - 1) else self.nextBookAction.setEnabled(True)
        self.nextChapAction.setEnabled(False) if self.chapterCombo.currentIndex() == (self.chapterCombo.count() - 1) else self.nextChapAction.setEnabled(True)

    def resetSearchWindow(self):
        """clears the last search and restores the search window for a new search each time the program is launched."""
        self.searchFromCombo.clear()
        self.searchToCombo.clear()
        self.searchFromCombo.addItems([dict(i.items())["bname"] for i in self.bible.bibleBooks])
        self.searchToCombo.addItems([dict(i.items())["bname"] for i in self.bible.bibleBooks])
        self.searchToCombo.setCurrentIndex(self.searchToCombo.count() - 1)
        self.connect(self.searchButton, SIGNAL("clicked()"), self.searchForWord)
        self.connect(self.searchList, SIGNAL("itemSelectionChanged()"), self.search.showVerse)
        self.connect(self.searchList, SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.search.goto)

    def searchForWord(self):
        """Authenticates the search key by ensuring that a valid search key i.e String, was entered"""

        word = self.searchEdit.text()
        if word:
            self.searchList.clear()
            self.searchBrowser.clear()
            start = self.searchFromCombo.currentIndex()
            end = self.searchToCombo.currentIndex()
            self.mySearch.search(self.search.searchWord, word, start, end)
            QThreadPool.globalInstance().start(self.mySearch)
            QApplication.processEvents()

    def updateCurrentFont(self):
        """Updates the current font to the newly selected font.
        This font changing only affects the central widgets i.e the Bible and Compare tabs.
        The other widgets retain their default fonts."""
        font = self.fontName.currentFont()
        size = self.fontSize.currentText()
        font.setPointSize(int(size))
        font.setBold(self.fontBoldAction.isChecked())
        font.setItalic(self.fontItalicAction.isChecked())
        self.bible.booksOpen[self.centralTab.currentIndex()][1].setFont(font)
        self.compareTable.setFont(font)

    def initBookmarks(self):
        """Loads saved bookmarks and connects appropriate actions to each bookmark."""
        self.bookmark.loadBookmarks()
        self.connect(self.bookmarksList, SIGNAL("itemDoubleClicked(QListWidgetItem *)"), self.bookmark.goto)

    def openPrevBook(self):
        """Opens previoius book of the current bible."""
        self.bookCombo.setCurrentIndex(self.bookCombo.currentIndex() - 1)

    def openNextBook(self):
        """ Opens next book of the current bible."""
        self.bookCombo.setCurrentIndex(self.bookCombo.currentIndex() + 1)

    def openPrevChap(self):
        """Opens the previous Chapter of the current book."""
        self.chapterCombo.setCurrentIndex(self.chapterCombo.currentIndex() - 1)

    def openNextChap(self):
        """ Opens the next Chapter of the current book."""
        self.chapterCombo.setCurrentIndex(self.chapterCombo.currentIndex() + 1)

    def closeEvent(self, event):
        """Overrides the close event.
        Ensures that the current state of the program is saved before exiting.
        @param event the close event."""
        self.bookmarks = self.bookmark.bookmarkList
        settings = QSettings()
        settings.setValue("mainwindow/geometry", self.saveGeometry())
        settings.setValue("mainwindow/state", self.saveState())
        settings.setValue("bookmarks", "{0}".format(self.bookmarks))
        settings.setValue("gotoCombos", "{0} {1} {2}".format(self.bookCombo.currentIndex(), self.chapterCombo.currentIndex(), self.verseCombo.currentIndex()))
        settings.setValue("font/family", self.fontName.currentFont().family())
        settings.setValue("font/size", self.fontSize.currentText())
        settings.setValue("font/bold", self.fontBoldAction.isChecked())
        settings.setValue("font/italic", self.fontItalicAction.isChecked())


class MySearch(QRunnable):
    def search(self, mth, word, start, end):
        self.mth = mth
        self.word = word
        self.start = start
        self.end = end
    def run(self):
        self.mth(self.word, self.start, self.end)
