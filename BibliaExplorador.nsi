############################################################################################
#      NSIS Installation Script created by NSIS Quick Setup Script Generator v1.09.18
#               Entirely Edited with NullSoft Scriptable Installation System                
#              by Vlasis K. Barkas aka Red Wine red_wine@freemail.gr Sep 2006               
############################################################################################

!define APP_NAME "BibliaExplorador"
!define COMP_NAME "Curious Minds"
!define WEB_SITE "http://www.facebook.com/adiksonliner"
!define VERSION "1.01.00.00"
!define COPYRIGHT "Curious Minds"
!define DESCRIPTION "A complete and sophisticated Bible Software"
!define INSTALLER_NAME "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\BibliaExplorador.exe"
!define MAIN_APP_EXE "Biblia.exe"
!define INSTALL_TYPE "SetShellVarContext current"
!define REG_ROOT "HKCU"
!define REG_APP_PATH "Software\Microsoft\Windows\CurrentVersion\App Paths\${MAIN_APP_EXE}"
!define UNINSTALL_PATH "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"

######################################################################

VIProductVersion  "${VERSION}"
VIAddVersionKey "ProductName"  "${APP_NAME}"
VIAddVersionKey "CompanyName"  "${COMP_NAME}"
VIAddVersionKey "LegalCopyright"  "${COPYRIGHT}"
VIAddVersionKey "FileDescription"  "${DESCRIPTION}"
VIAddVersionKey "FileVersion"  "${VERSION}"

######################################################################

SetCompressor ZLIB
Name "${APP_NAME}"
Caption "${APP_NAME}"
OutFile "${INSTALLER_NAME}"
BrandingText "${APP_NAME}"
XPStyle on
InstallDirRegKey "${REG_ROOT}" "${REG_APP_PATH}" ""
InstallDir "$PROGRAMFILES\BibliaExplorador"

######################################################################

!include "MUI.nsh"

!define MUI_ABORTWARNING
!define MUI_UNABORTWARNING

!insertmacro MUI_PAGE_WELCOME

!ifdef LICENSE_TXT
!insertmacro MUI_PAGE_LICENSE "${LICENSE_TXT}"
!endif

!insertmacro MUI_PAGE_DIRECTORY

!ifdef REG_START_MENU
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "BibliaExplorador"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${REG_ROOT}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${UNINSTALL_PATH}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${REG_START_MENU}"
!insertmacro MUI_PAGE_STARTMENU Application $SM_Folder
!endif

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN "$INSTDIR\${MAIN_APP_EXE}"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

######################################################################

Section -MainProgram
${INSTALL_TYPE}
SetOverwrite ifnewer
SetOutPath "$INSTDIR"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\Biblia.exe"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\bz2.pyd"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\pyexpat.pyd"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\PyQt4.QtCore.pyd"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\PyQt4.QtGui.pyd"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\python27.dll"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\QtCore4.dll"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\QtGui4.dll"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\sip.pyd"
File "C:\Users\ADIKSONLINE\Documents\Eric4Projects\Branch\BibliaExplorador\dist\unicodedata.pyd"
SectionEnd

######################################################################

Section -Additional
SetOutPath "$PROFILE\.bibliaexplorador"
SetOutPath "$PROFILE\.bibliaexplorador\Settings"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Settings\books.xml"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Settings\mycss.css"
SetOutPath "$PROFILE\.bibliaexplorador\Notes"
SetOutPath "$PROFILE\.bibliaexplorador\Bibles"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Bibles\AKJV.ont"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Bibles\ASV.ont"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Bibles\BBE.ont"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Bibles\KJV.ont"
File "C:\Users\ADIKSONLINE\.bibliaexplorador\Bibles\YLT.ont"
SectionEnd

######################################################################

Section -Icons_Reg
SetOutPath "$INSTDIR"
WriteUninstaller "$INSTDIR\uninstall.exe"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
CreateDirectory "$SMPROGRAMS\$SM_Folder"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!insertmacro MUI_STARTMENU_WRITE_END
!endif

!ifndef REG_START_MENU
CreateDirectory "$SMPROGRAMS\BibliaExplorador"
CreateShortCut "$SMPROGRAMS\BibliaExplorador\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$DESKTOP\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\BibliaExplorador\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\BibliaExplorador\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!endif

WriteRegStr ${REG_ROOT} "${REG_APP_PATH}" "" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayName" "${APP_NAME}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "UninstallString" "$INSTDIR\uninstall.exe"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayIcon" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayVersion" "${VERSION}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "Publisher" "${COMP_NAME}"

!ifdef WEB_SITE
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "URLInfoAbout" "${WEB_SITE}"
!endif
SectionEnd

######################################################################

Section Uninstall
${INSTALL_TYPE}
Delete "$INSTDIR\Biblia.exe"
Delete "$INSTDIR\bz2.pyd"
Delete "$INSTDIR\pyexpat.pyd"
Delete "$INSTDIR\PyQt4.QtCore.pyd"
Delete "$INSTDIR\PyQt4.QtGui.pyd"
Delete "$INSTDIR\python27.dll"
Delete "$INSTDIR\QtCore4.dll"
Delete "$INSTDIR\QtGui4.dll"
Delete "$INSTDIR\sip.pyd"
Delete "$INSTDIR\unicodedata.pyd"
Delete "$INSTDIR\uninstall.exe"
!ifdef WEB_SITE
Delete "$INSTDIR\${APP_NAME} website.url"
!endif

RmDir "$INSTDIR"

!ifndef NEVER_UNINSTALL
Delete "$PROFILE\.bibliaexplorador\Settings\books.xml"
Delete "$PROFILE\.bibliaexplorador\Settings\mycss.css"
Delete "$PROFILE\.bibliaexplorador\Bibles\AKJV.ont"
Delete "$PROFILE\.bibliaexplorador\Bibles\ASV.ont"
Delete "$PROFILE\.bibliaexplorador\Bibles\BBE.ont"
Delete "$PROFILE\.bibliaexplorador\Bibles\KJV.ont"
Delete "$PROFILE\.bibliaexplorador\Bibles\YLT.ont"
 
RmDir "$PROFILE\.bibliaexplorador\Bibles"
RmDir "$PROFILE\.bibliaexplorador\Notes"
RmDir "$PROFILE\.bibliaexplorador\Settings"
RMDir /r "$PROFILE\.bibliaexplorador"
!endif

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_GETFOLDER "Application" $SM_Folder
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk"
!endif
Delete "$DESKTOP\${APP_NAME}.lnk"

RmDir "$SMPROGRAMS\$SM_Folder"
!endif

!ifndef REG_START_MENU
Delete "$SMPROGRAMS\BibliaExplorador\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\BibliaExplorador\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\BibliaExplorador\${APP_NAME} Website.lnk"
!endif
Delete "$DESKTOP\${APP_NAME}.lnk"

RmDir "$SMPROGRAMS\BibliaExplorador"
!endif

DeleteRegKey ${REG_ROOT} "${REG_APP_PATH}"
DeleteRegKey ${REG_ROOT} "${UNINSTALL_PATH}"
SectionEnd

######################################################################

