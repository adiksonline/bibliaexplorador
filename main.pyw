#! /usr/bin/python
# -*- coding: utf8 -*-
"""This is the main module."""

import sip, sys, atexit
from encodings import ascii, utf_8
from biblia import *

class MSplashScreen(QSplashScreen):
    def __init__(self, parent = None):
        super(MSplashScreen, self).__init__(parent)

    def showMessage(self, string, align = Qt.AlignLeft | Qt.AlignBottom):
        """
        This method aligns the shown message to the left bottom except explicitly stated.
        """
        super(MSplashScreen, self).showMessage(string, align)

def main():
    """
    Creats an object of the Biblia class and executes it."""
    QApplication.setStyle(QStyleFactory.create("Cleanlooks"))
    app = QApplication(sys.argv)
    app.setApplicationName("BibliaExplorador")
    app.setOrganizationName("ADIKSonline")
    app.setOrganizationDomain("adiksonline.xtgem.com")
    app.setWindowIcon(QIcon(":/appicon.png"))
    splash = MSplashScreen()
    pixmap = QPixmap(":/splashscreen.png")
    splash.setPixmap(pixmap)
    splash.show()
    splash.showMessage("Importing required Modules . . .")
    app.processEvents()
    bible = Biblia(splash)
    splash.showMessage("Launching Application . . .")
    splash.finish(bible)
    bible.show()
    app.exec_()

try:
    main()
except Exception as e:
    print e
    QMessageBox.critical(None, "Critical Error", "AN ERROR OCCURED!\n{0}".format(e))
    sys.exit(1)
